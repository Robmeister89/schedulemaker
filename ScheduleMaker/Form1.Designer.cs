﻿namespace ScheduleMaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.team1txt = new System.Windows.Forms.TextBox();
            this.team2txt = new System.Windows.Forms.TextBox();
            this.team3txt = new System.Windows.Forms.TextBox();
            this.team4txt = new System.Windows.Forms.TextBox();
            this.team8txt = new System.Windows.Forms.TextBox();
            this.team7txt = new System.Windows.Forms.TextBox();
            this.team6txt = new System.Windows.Forms.TextBox();
            this.team5txt = new System.Windows.Forms.TextBox();
            this.team12txt = new System.Windows.Forms.TextBox();
            this.team11txt = new System.Windows.Forms.TextBox();
            this.team10txt = new System.Windows.Forms.TextBox();
            this.team9txt = new System.Windows.Forms.TextBox();
            this.team16txt = new System.Windows.Forms.TextBox();
            this.team15txt = new System.Windows.Forms.TextBox();
            this.team14txt = new System.Windows.Forms.TextBox();
            this.team13txt = new System.Windows.Forms.TextBox();
            this.team32txt = new System.Windows.Forms.TextBox();
            this.team31txt = new System.Windows.Forms.TextBox();
            this.team30txt = new System.Windows.Forms.TextBox();
            this.team29txt = new System.Windows.Forms.TextBox();
            this.team28txt = new System.Windows.Forms.TextBox();
            this.team27txt = new System.Windows.Forms.TextBox();
            this.team26txt = new System.Windows.Forms.TextBox();
            this.team25txt = new System.Windows.Forms.TextBox();
            this.team24txt = new System.Windows.Forms.TextBox();
            this.team23txt = new System.Windows.Forms.TextBox();
            this.team22txt = new System.Windows.Forms.TextBox();
            this.team21txt = new System.Windows.Forms.TextBox();
            this.team20txt = new System.Windows.Forms.TextBox();
            this.team19txt = new System.Windows.Forms.TextBox();
            this.team18txt = new System.Windows.Forms.TextBox();
            this.team17txt = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.div1con = new System.Windows.Forms.ComboBox();
            this.div1non = new System.Windows.Forms.ComboBox();
            this.div2non = new System.Windows.Forms.ComboBox();
            this.div2con = new System.Windows.Forms.ComboBox();
            this.div3non = new System.Windows.Forms.ComboBox();
            this.div3con = new System.Windows.Forms.ComboBox();
            this.div4non = new System.Windows.Forms.ComboBox();
            this.div4con = new System.Windows.Forms.ComboBox();
            this.div5non = new System.Windows.Forms.ComboBox();
            this.div5con = new System.Windows.Forms.ComboBox();
            this.div6non = new System.Windows.Forms.ComboBox();
            this.div6con = new System.Windows.Forms.ComboBox();
            this.div7non = new System.Windows.Forms.ComboBox();
            this.div7con = new System.Windows.Forms.ComboBox();
            this.div8non = new System.Windows.Forms.ComboBox();
            this.div8con = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.team17 = new System.Windows.Forms.TextBox();
            this.team18 = new System.Windows.Forms.TextBox();
            this.team19 = new System.Windows.Forms.TextBox();
            this.team20 = new System.Windows.Forms.TextBox();
            this.team21 = new System.Windows.Forms.TextBox();
            this.team22 = new System.Windows.Forms.TextBox();
            this.team23 = new System.Windows.Forms.TextBox();
            this.team24 = new System.Windows.Forms.TextBox();
            this.team28 = new System.Windows.Forms.TextBox();
            this.team27 = new System.Windows.Forms.TextBox();
            this.team26 = new System.Windows.Forms.TextBox();
            this.team25 = new System.Windows.Forms.TextBox();
            this.team32 = new System.Windows.Forms.TextBox();
            this.team31 = new System.Windows.Forms.TextBox();
            this.team30 = new System.Windows.Forms.TextBox();
            this.team29 = new System.Windows.Forms.TextBox();
            this.team16 = new System.Windows.Forms.TextBox();
            this.team15 = new System.Windows.Forms.TextBox();
            this.team14 = new System.Windows.Forms.TextBox();
            this.team13 = new System.Windows.Forms.TextBox();
            this.team12 = new System.Windows.Forms.TextBox();
            this.team11 = new System.Windows.Forms.TextBox();
            this.team10 = new System.Windows.Forms.TextBox();
            this.team9 = new System.Windows.Forms.TextBox();
            this.team8 = new System.Windows.Forms.TextBox();
            this.team7 = new System.Windows.Forms.TextBox();
            this.team6 = new System.Windows.Forms.TextBox();
            this.team5 = new System.Windows.Forms.TextBox();
            this.team4 = new System.Windows.Forms.TextBox();
            this.team3 = new System.Windows.Forms.TextBox();
            this.team2 = new System.Windows.Forms.TextBox();
            this.team1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // team1txt
            // 
            this.team1txt.Enabled = false;
            this.team1txt.Location = new System.Drawing.Point(167, 72);
            this.team1txt.Name = "team1txt";
            this.team1txt.Size = new System.Drawing.Size(40, 20);
            this.team1txt.TabIndex = 0;
            // 
            // team2txt
            // 
            this.team2txt.Enabled = false;
            this.team2txt.Location = new System.Drawing.Point(167, 98);
            this.team2txt.Name = "team2txt";
            this.team2txt.Size = new System.Drawing.Size(40, 20);
            this.team2txt.TabIndex = 2;
            // 
            // team3txt
            // 
            this.team3txt.Enabled = false;
            this.team3txt.Location = new System.Drawing.Point(167, 124);
            this.team3txt.Name = "team3txt";
            this.team3txt.Size = new System.Drawing.Size(40, 20);
            this.team3txt.TabIndex = 4;
            // 
            // team4txt
            // 
            this.team4txt.Enabled = false;
            this.team4txt.Location = new System.Drawing.Point(167, 150);
            this.team4txt.Name = "team4txt";
            this.team4txt.Size = new System.Drawing.Size(40, 20);
            this.team4txt.TabIndex = 6;
            // 
            // team8txt
            // 
            this.team8txt.Enabled = false;
            this.team8txt.Location = new System.Drawing.Point(167, 274);
            this.team8txt.Name = "team8txt";
            this.team8txt.Size = new System.Drawing.Size(40, 20);
            this.team8txt.TabIndex = 14;
            // 
            // team7txt
            // 
            this.team7txt.Enabled = false;
            this.team7txt.Location = new System.Drawing.Point(167, 248);
            this.team7txt.Name = "team7txt";
            this.team7txt.Size = new System.Drawing.Size(40, 20);
            this.team7txt.TabIndex = 12;
            // 
            // team6txt
            // 
            this.team6txt.Enabled = false;
            this.team6txt.Location = new System.Drawing.Point(167, 222);
            this.team6txt.Name = "team6txt";
            this.team6txt.Size = new System.Drawing.Size(40, 20);
            this.team6txt.TabIndex = 10;
            // 
            // team5txt
            // 
            this.team5txt.Enabled = false;
            this.team5txt.Location = new System.Drawing.Point(167, 196);
            this.team5txt.Name = "team5txt";
            this.team5txt.Size = new System.Drawing.Size(40, 20);
            this.team5txt.TabIndex = 8;
            // 
            // team12txt
            // 
            this.team12txt.Enabled = false;
            this.team12txt.Location = new System.Drawing.Point(167, 401);
            this.team12txt.Name = "team12txt";
            this.team12txt.Size = new System.Drawing.Size(40, 20);
            this.team12txt.TabIndex = 22;
            // 
            // team11txt
            // 
            this.team11txt.Enabled = false;
            this.team11txt.Location = new System.Drawing.Point(167, 375);
            this.team11txt.Name = "team11txt";
            this.team11txt.Size = new System.Drawing.Size(40, 20);
            this.team11txt.TabIndex = 20;
            // 
            // team10txt
            // 
            this.team10txt.Enabled = false;
            this.team10txt.Location = new System.Drawing.Point(167, 349);
            this.team10txt.Name = "team10txt";
            this.team10txt.Size = new System.Drawing.Size(40, 20);
            this.team10txt.TabIndex = 18;
            // 
            // team9txt
            // 
            this.team9txt.Enabled = false;
            this.team9txt.Location = new System.Drawing.Point(167, 323);
            this.team9txt.Name = "team9txt";
            this.team9txt.Size = new System.Drawing.Size(40, 20);
            this.team9txt.TabIndex = 16;
            // 
            // team16txt
            // 
            this.team16txt.Enabled = false;
            this.team16txt.Location = new System.Drawing.Point(167, 522);
            this.team16txt.Name = "team16txt";
            this.team16txt.Size = new System.Drawing.Size(40, 20);
            this.team16txt.TabIndex = 30;
            // 
            // team15txt
            // 
            this.team15txt.Enabled = false;
            this.team15txt.Location = new System.Drawing.Point(167, 496);
            this.team15txt.Name = "team15txt";
            this.team15txt.Size = new System.Drawing.Size(40, 20);
            this.team15txt.TabIndex = 28;
            // 
            // team14txt
            // 
            this.team14txt.Enabled = false;
            this.team14txt.Location = new System.Drawing.Point(167, 470);
            this.team14txt.Name = "team14txt";
            this.team14txt.Size = new System.Drawing.Size(40, 20);
            this.team14txt.TabIndex = 26;
            // 
            // team13txt
            // 
            this.team13txt.Enabled = false;
            this.team13txt.Location = new System.Drawing.Point(167, 444);
            this.team13txt.Name = "team13txt";
            this.team13txt.Size = new System.Drawing.Size(40, 20);
            this.team13txt.TabIndex = 24;
            // 
            // team32txt
            // 
            this.team32txt.Enabled = false;
            this.team32txt.Location = new System.Drawing.Point(541, 522);
            this.team32txt.Name = "team32txt";
            this.team32txt.Size = new System.Drawing.Size(40, 20);
            this.team32txt.TabIndex = 62;
            // 
            // team31txt
            // 
            this.team31txt.Enabled = false;
            this.team31txt.Location = new System.Drawing.Point(541, 496);
            this.team31txt.Name = "team31txt";
            this.team31txt.Size = new System.Drawing.Size(40, 20);
            this.team31txt.TabIndex = 60;
            // 
            // team30txt
            // 
            this.team30txt.Enabled = false;
            this.team30txt.Location = new System.Drawing.Point(541, 470);
            this.team30txt.Name = "team30txt";
            this.team30txt.Size = new System.Drawing.Size(40, 20);
            this.team30txt.TabIndex = 58;
            // 
            // team29txt
            // 
            this.team29txt.Enabled = false;
            this.team29txt.Location = new System.Drawing.Point(541, 444);
            this.team29txt.Name = "team29txt";
            this.team29txt.Size = new System.Drawing.Size(40, 20);
            this.team29txt.TabIndex = 56;
            // 
            // team28txt
            // 
            this.team28txt.Enabled = false;
            this.team28txt.Location = new System.Drawing.Point(541, 401);
            this.team28txt.Name = "team28txt";
            this.team28txt.Size = new System.Drawing.Size(40, 20);
            this.team28txt.TabIndex = 54;
            // 
            // team27txt
            // 
            this.team27txt.Enabled = false;
            this.team27txt.Location = new System.Drawing.Point(541, 375);
            this.team27txt.Name = "team27txt";
            this.team27txt.Size = new System.Drawing.Size(40, 20);
            this.team27txt.TabIndex = 52;
            // 
            // team26txt
            // 
            this.team26txt.Enabled = false;
            this.team26txt.Location = new System.Drawing.Point(541, 349);
            this.team26txt.Name = "team26txt";
            this.team26txt.Size = new System.Drawing.Size(40, 20);
            this.team26txt.TabIndex = 50;
            // 
            // team25txt
            // 
            this.team25txt.Enabled = false;
            this.team25txt.Location = new System.Drawing.Point(541, 323);
            this.team25txt.Name = "team25txt";
            this.team25txt.Size = new System.Drawing.Size(40, 20);
            this.team25txt.TabIndex = 48;
            // 
            // team24txt
            // 
            this.team24txt.Enabled = false;
            this.team24txt.Location = new System.Drawing.Point(541, 274);
            this.team24txt.Name = "team24txt";
            this.team24txt.Size = new System.Drawing.Size(40, 20);
            this.team24txt.TabIndex = 46;
            // 
            // team23txt
            // 
            this.team23txt.Enabled = false;
            this.team23txt.Location = new System.Drawing.Point(541, 248);
            this.team23txt.Name = "team23txt";
            this.team23txt.Size = new System.Drawing.Size(40, 20);
            this.team23txt.TabIndex = 44;
            // 
            // team22txt
            // 
            this.team22txt.Enabled = false;
            this.team22txt.Location = new System.Drawing.Point(541, 222);
            this.team22txt.Name = "team22txt";
            this.team22txt.Size = new System.Drawing.Size(40, 20);
            this.team22txt.TabIndex = 42;
            // 
            // team21txt
            // 
            this.team21txt.Enabled = false;
            this.team21txt.Location = new System.Drawing.Point(541, 196);
            this.team21txt.Name = "team21txt";
            this.team21txt.Size = new System.Drawing.Size(40, 20);
            this.team21txt.TabIndex = 40;
            // 
            // team20txt
            // 
            this.team20txt.Enabled = false;
            this.team20txt.Location = new System.Drawing.Point(541, 150);
            this.team20txt.Name = "team20txt";
            this.team20txt.Size = new System.Drawing.Size(40, 20);
            this.team20txt.TabIndex = 38;
            // 
            // team19txt
            // 
            this.team19txt.Enabled = false;
            this.team19txt.Location = new System.Drawing.Point(541, 124);
            this.team19txt.Name = "team19txt";
            this.team19txt.Size = new System.Drawing.Size(40, 20);
            this.team19txt.TabIndex = 36;
            // 
            // team18txt
            // 
            this.team18txt.Enabled = false;
            this.team18txt.Location = new System.Drawing.Point(541, 98);
            this.team18txt.Name = "team18txt";
            this.team18txt.Size = new System.Drawing.Size(40, 20);
            this.team18txt.TabIndex = 34;
            // 
            // team17txt
            // 
            this.team17txt.Enabled = false;
            this.team17txt.Location = new System.Drawing.Point(541, 72);
            this.team17txt.Name = "team17txt";
            this.team17txt.Size = new System.Drawing.Size(40, 20);
            this.team17txt.TabIndex = 32;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(114, 53);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 13);
            this.label33.TabIndex = 64;
            this.label33.Text = "Division 1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(114, 176);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 65;
            this.label34.Text = "Division 2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(114, 303);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 13);
            this.label35.TabIndex = 66;
            this.label35.Text = "Division 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(114, 425);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 13);
            this.label36.TabIndex = 67;
            this.label36.Text = "Division 4";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(489, 425);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 13);
            this.label37.TabIndex = 71;
            this.label37.Text = "Division 8";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(489, 303);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 13);
            this.label38.TabIndex = 70;
            this.label38.Text = "Division 7";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(489, 176);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 13);
            this.label39.TabIndex = 69;
            this.label39.Text = "Division 6";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(489, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 68;
            this.label40.Text = "Division 5";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 75;
            this.button1.Text = "Generate Schedule";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // div1con
            // 
            this.div1con.FormattingEnabled = true;
            this.div1con.Items.AddRange(new object[] {
            "2",
            "3",
            "4"});
            this.div1con.Location = new System.Drawing.Point(219, 50);
            this.div1con.Name = "div1con";
            this.div1con.Size = new System.Drawing.Size(53, 21);
            this.div1con.TabIndex = 76;
            this.div1con.SelectedIndexChanged += new System.EventHandler(this.div1con_SelectedIndexChanged);
            // 
            // div1non
            // 
            this.div1non.FormattingEnabled = true;
            this.div1non.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.div1non.Location = new System.Drawing.Point(278, 50);
            this.div1non.Name = "div1non";
            this.div1non.Size = new System.Drawing.Size(53, 21);
            this.div1non.TabIndex = 77;
            this.div1non.SelectedIndexChanged += new System.EventHandler(this.div1non_SelectedIndexChanged);
            // 
            // div2non
            // 
            this.div2non.FormattingEnabled = true;
            this.div2non.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.div2non.Location = new System.Drawing.Point(278, 173);
            this.div2non.Name = "div2non";
            this.div2non.Size = new System.Drawing.Size(53, 21);
            this.div2non.TabIndex = 79;
            // 
            // div2con
            // 
            this.div2con.FormattingEnabled = true;
            this.div2con.Items.AddRange(new object[] {
            "1",
            "3",
            "4"});
            this.div2con.Location = new System.Drawing.Point(219, 173);
            this.div2con.Name = "div2con";
            this.div2con.Size = new System.Drawing.Size(53, 21);
            this.div2con.TabIndex = 78;
            // 
            // div3non
            // 
            this.div3non.FormattingEnabled = true;
            this.div3non.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.div3non.Location = new System.Drawing.Point(278, 300);
            this.div3non.Name = "div3non";
            this.div3non.Size = new System.Drawing.Size(53, 21);
            this.div3non.TabIndex = 81;
            // 
            // div3con
            // 
            this.div3con.FormattingEnabled = true;
            this.div3con.Items.AddRange(new object[] {
            "1",
            "2",
            "4"});
            this.div3con.Location = new System.Drawing.Point(219, 300);
            this.div3con.Name = "div3con";
            this.div3con.Size = new System.Drawing.Size(53, 21);
            this.div3con.TabIndex = 80;
            // 
            // div4non
            // 
            this.div4non.FormattingEnabled = true;
            this.div4non.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.div4non.Location = new System.Drawing.Point(278, 422);
            this.div4non.Name = "div4non";
            this.div4non.Size = new System.Drawing.Size(53, 21);
            this.div4non.TabIndex = 83;
            // 
            // div4con
            // 
            this.div4con.FormattingEnabled = true;
            this.div4con.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.div4con.Location = new System.Drawing.Point(219, 422);
            this.div4con.Name = "div4con";
            this.div4con.Size = new System.Drawing.Size(53, 21);
            this.div4con.TabIndex = 82;
            // 
            // div5non
            // 
            this.div5non.FormattingEnabled = true;
            this.div5non.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.div5non.Location = new System.Drawing.Point(654, 50);
            this.div5non.Name = "div5non";
            this.div5non.Size = new System.Drawing.Size(53, 21);
            this.div5non.TabIndex = 85;
            // 
            // div5con
            // 
            this.div5con.FormattingEnabled = true;
            this.div5con.Items.AddRange(new object[] {
            "6",
            "7",
            "8"});
            this.div5con.Location = new System.Drawing.Point(595, 50);
            this.div5con.Name = "div5con";
            this.div5con.Size = new System.Drawing.Size(53, 21);
            this.div5con.TabIndex = 84;
            // 
            // div6non
            // 
            this.div6non.FormattingEnabled = true;
            this.div6non.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.div6non.Location = new System.Drawing.Point(654, 173);
            this.div6non.Name = "div6non";
            this.div6non.Size = new System.Drawing.Size(53, 21);
            this.div6non.TabIndex = 87;
            // 
            // div6con
            // 
            this.div6con.FormattingEnabled = true;
            this.div6con.Items.AddRange(new object[] {
            "5",
            "7",
            "8"});
            this.div6con.Location = new System.Drawing.Point(595, 173);
            this.div6con.Name = "div6con";
            this.div6con.Size = new System.Drawing.Size(53, 21);
            this.div6con.TabIndex = 86;
            // 
            // div7non
            // 
            this.div7non.FormattingEnabled = true;
            this.div7non.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.div7non.Location = new System.Drawing.Point(654, 300);
            this.div7non.Name = "div7non";
            this.div7non.Size = new System.Drawing.Size(53, 21);
            this.div7non.TabIndex = 89;
            // 
            // div7con
            // 
            this.div7con.FormattingEnabled = true;
            this.div7con.Items.AddRange(new object[] {
            "5",
            "6",
            "8"});
            this.div7con.Location = new System.Drawing.Point(595, 300);
            this.div7con.Name = "div7con";
            this.div7con.Size = new System.Drawing.Size(53, 21);
            this.div7con.TabIndex = 88;
            // 
            // div8non
            // 
            this.div8non.FormattingEnabled = true;
            this.div8non.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.div8non.Location = new System.Drawing.Point(654, 422);
            this.div8non.Name = "div8non";
            this.div8non.Size = new System.Drawing.Size(53, 21);
            this.div8non.TabIndex = 91;
            // 
            // div8con
            // 
            this.div8con.FormattingEnabled = true;
            this.div8con.Items.AddRange(new object[] {
            "5",
            "6",
            "7"});
            this.div8con.Location = new System.Drawing.Point(595, 422);
            this.div8con.Name = "div8con";
            this.div8con.Size = new System.Drawing.Size(53, 21);
            this.div8con.TabIndex = 90;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(135, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 23);
            this.button2.TabIndex = 92;
            this.button2.Text = "Refresh Team Labels";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // team17
            // 
            this.team17.Location = new System.Drawing.Point(390, 72);
            this.team17.Name = "team17";
            this.team17.Size = new System.Drawing.Size(145, 20);
            this.team17.TabIndex = 93;
            // 
            // team18
            // 
            this.team18.Location = new System.Drawing.Point(390, 98);
            this.team18.Name = "team18";
            this.team18.Size = new System.Drawing.Size(145, 20);
            this.team18.TabIndex = 94;
            // 
            // team19
            // 
            this.team19.Location = new System.Drawing.Point(390, 124);
            this.team19.Name = "team19";
            this.team19.Size = new System.Drawing.Size(145, 20);
            this.team19.TabIndex = 95;
            // 
            // team20
            // 
            this.team20.Location = new System.Drawing.Point(390, 150);
            this.team20.Name = "team20";
            this.team20.Size = new System.Drawing.Size(145, 20);
            this.team20.TabIndex = 96;
            // 
            // team21
            // 
            this.team21.Location = new System.Drawing.Point(390, 196);
            this.team21.Name = "team21";
            this.team21.Size = new System.Drawing.Size(145, 20);
            this.team21.TabIndex = 97;
            // 
            // team22
            // 
            this.team22.Location = new System.Drawing.Point(390, 222);
            this.team22.Name = "team22";
            this.team22.Size = new System.Drawing.Size(145, 20);
            this.team22.TabIndex = 98;
            // 
            // team23
            // 
            this.team23.Location = new System.Drawing.Point(390, 248);
            this.team23.Name = "team23";
            this.team23.Size = new System.Drawing.Size(145, 20);
            this.team23.TabIndex = 99;
            // 
            // team24
            // 
            this.team24.Location = new System.Drawing.Point(390, 274);
            this.team24.Name = "team24";
            this.team24.Size = new System.Drawing.Size(145, 20);
            this.team24.TabIndex = 100;
            // 
            // team28
            // 
            this.team28.Location = new System.Drawing.Point(390, 401);
            this.team28.Name = "team28";
            this.team28.Size = new System.Drawing.Size(145, 20);
            this.team28.TabIndex = 104;
            // 
            // team27
            // 
            this.team27.Location = new System.Drawing.Point(390, 375);
            this.team27.Name = "team27";
            this.team27.Size = new System.Drawing.Size(145, 20);
            this.team27.TabIndex = 103;
            // 
            // team26
            // 
            this.team26.Location = new System.Drawing.Point(390, 349);
            this.team26.Name = "team26";
            this.team26.Size = new System.Drawing.Size(145, 20);
            this.team26.TabIndex = 102;
            // 
            // team25
            // 
            this.team25.Location = new System.Drawing.Point(390, 323);
            this.team25.Name = "team25";
            this.team25.Size = new System.Drawing.Size(145, 20);
            this.team25.TabIndex = 101;
            // 
            // team32
            // 
            this.team32.Location = new System.Drawing.Point(390, 522);
            this.team32.Name = "team32";
            this.team32.Size = new System.Drawing.Size(145, 20);
            this.team32.TabIndex = 108;
            // 
            // team31
            // 
            this.team31.Location = new System.Drawing.Point(390, 496);
            this.team31.Name = "team31";
            this.team31.Size = new System.Drawing.Size(145, 20);
            this.team31.TabIndex = 107;
            // 
            // team30
            // 
            this.team30.Location = new System.Drawing.Point(390, 470);
            this.team30.Name = "team30";
            this.team30.Size = new System.Drawing.Size(145, 20);
            this.team30.TabIndex = 106;
            // 
            // team29
            // 
            this.team29.Location = new System.Drawing.Point(390, 444);
            this.team29.Name = "team29";
            this.team29.Size = new System.Drawing.Size(145, 20);
            this.team29.TabIndex = 105;
            // 
            // team16
            // 
            this.team16.Location = new System.Drawing.Point(16, 522);
            this.team16.Name = "team16";
            this.team16.Size = new System.Drawing.Size(145, 20);
            this.team16.TabIndex = 124;
            // 
            // team15
            // 
            this.team15.Location = new System.Drawing.Point(16, 496);
            this.team15.Name = "team15";
            this.team15.Size = new System.Drawing.Size(145, 20);
            this.team15.TabIndex = 123;
            // 
            // team14
            // 
            this.team14.Location = new System.Drawing.Point(16, 470);
            this.team14.Name = "team14";
            this.team14.Size = new System.Drawing.Size(145, 20);
            this.team14.TabIndex = 122;
            // 
            // team13
            // 
            this.team13.Location = new System.Drawing.Point(16, 444);
            this.team13.Name = "team13";
            this.team13.Size = new System.Drawing.Size(145, 20);
            this.team13.TabIndex = 121;
            // 
            // team12
            // 
            this.team12.Location = new System.Drawing.Point(16, 401);
            this.team12.Name = "team12";
            this.team12.Size = new System.Drawing.Size(145, 20);
            this.team12.TabIndex = 120;
            // 
            // team11
            // 
            this.team11.Location = new System.Drawing.Point(16, 375);
            this.team11.Name = "team11";
            this.team11.Size = new System.Drawing.Size(145, 20);
            this.team11.TabIndex = 119;
            // 
            // team10
            // 
            this.team10.Location = new System.Drawing.Point(16, 349);
            this.team10.Name = "team10";
            this.team10.Size = new System.Drawing.Size(145, 20);
            this.team10.TabIndex = 118;
            // 
            // team9
            // 
            this.team9.Location = new System.Drawing.Point(16, 323);
            this.team9.Name = "team9";
            this.team9.Size = new System.Drawing.Size(145, 20);
            this.team9.TabIndex = 117;
            // 
            // team8
            // 
            this.team8.Location = new System.Drawing.Point(16, 274);
            this.team8.Name = "team8";
            this.team8.Size = new System.Drawing.Size(145, 20);
            this.team8.TabIndex = 116;
            // 
            // team7
            // 
            this.team7.Location = new System.Drawing.Point(16, 248);
            this.team7.Name = "team7";
            this.team7.Size = new System.Drawing.Size(145, 20);
            this.team7.TabIndex = 115;
            // 
            // team6
            // 
            this.team6.Location = new System.Drawing.Point(16, 222);
            this.team6.Name = "team6";
            this.team6.Size = new System.Drawing.Size(145, 20);
            this.team6.TabIndex = 114;
            // 
            // team5
            // 
            this.team5.Location = new System.Drawing.Point(16, 196);
            this.team5.Name = "team5";
            this.team5.Size = new System.Drawing.Size(145, 20);
            this.team5.TabIndex = 113;
            // 
            // team4
            // 
            this.team4.Location = new System.Drawing.Point(16, 150);
            this.team4.Name = "team4";
            this.team4.Size = new System.Drawing.Size(145, 20);
            this.team4.TabIndex = 112;
            // 
            // team3
            // 
            this.team3.Location = new System.Drawing.Point(16, 124);
            this.team3.Name = "team3";
            this.team3.Size = new System.Drawing.Size(145, 20);
            this.team3.TabIndex = 111;
            // 
            // team2
            // 
            this.team2.Location = new System.Drawing.Point(16, 98);
            this.team2.Name = "team2";
            this.team2.Size = new System.Drawing.Size(145, 20);
            this.team2.TabIndex = 110;
            // 
            // team1
            // 
            this.team1.Location = new System.Drawing.Point(16, 72);
            this.team1.Name = "team1";
            this.team1.Size = new System.Drawing.Size(145, 20);
            this.team1.TabIndex = 109;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(258, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 23);
            this.button3.TabIndex = 125;
            this.button3.Text = "Open Teams.csv";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 669);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.team16);
            this.Controls.Add(this.team15);
            this.Controls.Add(this.team14);
            this.Controls.Add(this.team13);
            this.Controls.Add(this.team12);
            this.Controls.Add(this.team11);
            this.Controls.Add(this.team10);
            this.Controls.Add(this.team9);
            this.Controls.Add(this.team8);
            this.Controls.Add(this.team7);
            this.Controls.Add(this.team6);
            this.Controls.Add(this.team5);
            this.Controls.Add(this.team4);
            this.Controls.Add(this.team3);
            this.Controls.Add(this.team2);
            this.Controls.Add(this.team1);
            this.Controls.Add(this.team32);
            this.Controls.Add(this.team31);
            this.Controls.Add(this.team30);
            this.Controls.Add(this.team29);
            this.Controls.Add(this.team28);
            this.Controls.Add(this.team27);
            this.Controls.Add(this.team26);
            this.Controls.Add(this.team25);
            this.Controls.Add(this.team24);
            this.Controls.Add(this.team23);
            this.Controls.Add(this.team22);
            this.Controls.Add(this.team21);
            this.Controls.Add(this.team20);
            this.Controls.Add(this.team19);
            this.Controls.Add(this.team18);
            this.Controls.Add(this.team17);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.div8non);
            this.Controls.Add(this.div8con);
            this.Controls.Add(this.div7non);
            this.Controls.Add(this.div7con);
            this.Controls.Add(this.div6non);
            this.Controls.Add(this.div6con);
            this.Controls.Add(this.div5non);
            this.Controls.Add(this.div5con);
            this.Controls.Add(this.div4non);
            this.Controls.Add(this.div4con);
            this.Controls.Add(this.div3non);
            this.Controls.Add(this.div3con);
            this.Controls.Add(this.div2non);
            this.Controls.Add(this.div2con);
            this.Controls.Add(this.div1non);
            this.Controls.Add(this.div1con);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.team32txt);
            this.Controls.Add(this.team31txt);
            this.Controls.Add(this.team30txt);
            this.Controls.Add(this.team29txt);
            this.Controls.Add(this.team28txt);
            this.Controls.Add(this.team27txt);
            this.Controls.Add(this.team26txt);
            this.Controls.Add(this.team25txt);
            this.Controls.Add(this.team24txt);
            this.Controls.Add(this.team23txt);
            this.Controls.Add(this.team22txt);
            this.Controls.Add(this.team21txt);
            this.Controls.Add(this.team20txt);
            this.Controls.Add(this.team19txt);
            this.Controls.Add(this.team18txt);
            this.Controls.Add(this.team17txt);
            this.Controls.Add(this.team16txt);
            this.Controls.Add(this.team15txt);
            this.Controls.Add(this.team14txt);
            this.Controls.Add(this.team13txt);
            this.Controls.Add(this.team12txt);
            this.Controls.Add(this.team11txt);
            this.Controls.Add(this.team10txt);
            this.Controls.Add(this.team9txt);
            this.Controls.Add(this.team8txt);
            this.Controls.Add(this.team7txt);
            this.Controls.Add(this.team6txt);
            this.Controls.Add(this.team5txt);
            this.Controls.Add(this.team4txt);
            this.Controls.Add(this.team3txt);
            this.Controls.Add(this.team2txt);
            this.Controls.Add(this.team1txt);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Draft Day Sports Pro Football Mod || 32-Team Custom Format Schedule Generator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox team1txt;
        private System.Windows.Forms.TextBox team2txt;
        private System.Windows.Forms.TextBox team3txt;
        private System.Windows.Forms.TextBox team4txt;
        private System.Windows.Forms.TextBox team8txt;
        private System.Windows.Forms.TextBox team7txt;
        private System.Windows.Forms.TextBox team6txt;
        private System.Windows.Forms.TextBox team5txt;
        private System.Windows.Forms.TextBox team12txt;
        private System.Windows.Forms.TextBox team11txt;
        private System.Windows.Forms.TextBox team10txt;
        private System.Windows.Forms.TextBox team9txt;
        private System.Windows.Forms.TextBox team16txt;
        private System.Windows.Forms.TextBox team15txt;
        private System.Windows.Forms.TextBox team14txt;
        private System.Windows.Forms.TextBox team13txt;
        private System.Windows.Forms.TextBox team32txt;
        private System.Windows.Forms.TextBox team31txt;
        private System.Windows.Forms.TextBox team30txt;
        private System.Windows.Forms.TextBox team29txt;
        private System.Windows.Forms.TextBox team28txt;
        private System.Windows.Forms.TextBox team27txt;
        private System.Windows.Forms.TextBox team26txt;
        private System.Windows.Forms.TextBox team25txt;
        private System.Windows.Forms.TextBox team24txt;
        private System.Windows.Forms.TextBox team23txt;
        private System.Windows.Forms.TextBox team22txt;
        private System.Windows.Forms.TextBox team21txt;
        private System.Windows.Forms.TextBox team20txt;
        private System.Windows.Forms.TextBox team19txt;
        private System.Windows.Forms.TextBox team18txt;
        private System.Windows.Forms.TextBox team17txt;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox div1con;
        private System.Windows.Forms.ComboBox div1non;
        private System.Windows.Forms.ComboBox div2non;
        private System.Windows.Forms.ComboBox div2con;
        private System.Windows.Forms.ComboBox div3non;
        private System.Windows.Forms.ComboBox div3con;
        private System.Windows.Forms.ComboBox div4non;
        private System.Windows.Forms.ComboBox div4con;
        private System.Windows.Forms.ComboBox div5non;
        private System.Windows.Forms.ComboBox div5con;
        private System.Windows.Forms.ComboBox div6non;
        private System.Windows.Forms.ComboBox div6con;
        private System.Windows.Forms.ComboBox div7non;
        private System.Windows.Forms.ComboBox div7con;
        private System.Windows.Forms.ComboBox div8non;
        private System.Windows.Forms.ComboBox div8con;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox team17;
        private System.Windows.Forms.TextBox team18;
        private System.Windows.Forms.TextBox team19;
        private System.Windows.Forms.TextBox team20;
        private System.Windows.Forms.TextBox team21;
        private System.Windows.Forms.TextBox team22;
        private System.Windows.Forms.TextBox team23;
        private System.Windows.Forms.TextBox team24;
        private System.Windows.Forms.TextBox team28;
        private System.Windows.Forms.TextBox team27;
        private System.Windows.Forms.TextBox team26;
        private System.Windows.Forms.TextBox team25;
        private System.Windows.Forms.TextBox team32;
        private System.Windows.Forms.TextBox team31;
        private System.Windows.Forms.TextBox team30;
        private System.Windows.Forms.TextBox team29;
        private System.Windows.Forms.TextBox team16;
        private System.Windows.Forms.TextBox team15;
        private System.Windows.Forms.TextBox team14;
        private System.Windows.Forms.TextBox team13;
        private System.Windows.Forms.TextBox team12;
        private System.Windows.Forms.TextBox team11;
        private System.Windows.Forms.TextBox team10;
        private System.Windows.Forms.TextBox team9;
        private System.Windows.Forms.TextBox team8;
        private System.Windows.Forms.TextBox team7;
        private System.Windows.Forms.TextBox team6;
        private System.Windows.Forms.TextBox team5;
        private System.Windows.Forms.TextBox team4;
        private System.Windows.Forms.TextBox team3;
        private System.Windows.Forms.TextBox team2;
        private System.Windows.Forms.TextBox team1;
        private System.Windows.Forms.Button button3;
    }
}

