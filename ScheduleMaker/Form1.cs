﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScheduleMaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (ComboBox c in this.Controls.OfType<ComboBox>())
                c.Enabled = false;
            div1con.Enabled = true;
            div1non.Enabled = true;

            for (int i = 1; i <= 32; i++)
            {
                this.Controls["team" + i.ToString()].Text = Properties.Settings.Default["team" + i.ToString()].ToString();
            }

            for (int i = 1; i <= 8; i++)
            {
                this.Controls["div" + i.ToString() + "con"].Text = Properties.Settings.Default["div" + i.ToString() + "con"].ToString();
                this.Controls["div" + i.ToString() + "non"].Text = Properties.Settings.Default["div" + i.ToString() + "non"].ToString();
            }
        }

        // convert csv to list of teams... can be modded but should not be more or less than 32 teams as the schedule only works for 32 team/custom DDS format
        private List<Team> TeamsFromCsv
        {
            get
            {
                List<Team> teams = new List<Team>();
                string filePath = Path.GetDirectoryName(Application.ExecutablePath) + "//Teams.csv";
                StreamReader reader = new StreamReader(filePath);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    String[] tokens = line.Split(',');
                    Team team = new Team()
                    {
                        TeamName = tokens[0],
                        TeamID = tokens[1]
                    };
                    teams.Add(team);
                }
                return teams;
            }
        }

        private string teamsFilePath = $"{Path.GetDirectoryName(Application.ExecutablePath)}/Teams.csv";
        private string schedFilePath = $"{Path.GetDirectoryName(Application.ExecutablePath)}/newSchedule.csv";

        // write schedule to csv file for DDS import
        private void button1_Click(object sender, EventArgs e)
        {
            button2.PerformClick();
            StringBuilder sbOutput = new StringBuilder();

            List<string> Schedule = new List<string>();
            Schedule.AddRange(Pre1); Schedule.AddRange(Pre2);
            Schedule.AddRange(Week1); Schedule.AddRange(Week2); Schedule.AddRange(Week3); Schedule.AddRange(Week4);
            Schedule.AddRange(Week5); Schedule.AddRange(Week6); Schedule.AddRange(Week7); Schedule.AddRange(Week8);
            Schedule.AddRange(Week9); Schedule.AddRange(Week10); Schedule.AddRange(Week11); Schedule.AddRange(Week12);
            Schedule.AddRange(Week13); Schedule.AddRange(Week14); Schedule.AddRange(Week15); Schedule.AddRange(Week16);

            foreach (string game in Schedule)
            {
                sbOutput.AppendLine(game);
            }

            // maybe allow the user to save it on their own???
            if (File.Exists(schedFilePath))
                try
                {
                    File.Delete(schedFilePath);
                    File.WriteAllText(schedFilePath, sbOutput.ToString());
                    MessageBox.Show("Schedule Exported!");
                    SaveSettings();
                }
                catch
                {
                    MessageBox.Show("Please close 'newSchedule.csv' to continue.");
                }
            else
            {
                File.WriteAllText(schedFilePath, sbOutput.ToString());
                MessageBox.Show("Schedule Generated!");
                SaveSettings();
            }
        }

        // save settings from previous schedule
        private void SaveSettings()
        {
            for (int i = 1; i <= 32; i++)
            {
                Properties.Settings.Default["team" + i.ToString()] = this.Controls["team" + i.ToString()].Text;
            }
            for (int i = 1; i <= 8; i++)
            {
                Properties.Settings.Default["div" + i.ToString() + "con"] = this.Controls["div" + i.ToString() + "con"].Text;
                Properties.Settings.Default["div" + i.ToString() + "non"] = this.Controls["div" + i.ToString() + "non"].Text;
            }
            Properties.Settings.Default.Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= TeamsFromCsv.Count; i++)
            {
                Team team = TeamsFromCsv.Where(t => t.TeamName == this.Controls["team" + i.ToString()].Text).FirstOrDefault();
                this.Controls["team" + i.ToString() + "txt"].Text = team.TeamID;
            }
            SetTeams();
        }

        private string Team1, Team2, Team3, Team4, Team5, Team6, Team7, Team8, Team9, Team10, Team11, Team12, Team13, Team14, Team15, Team16, Team17, Team18, Team19, Team20, Team21, Team22, Team23, Team24, Team25, Team26, Team27, Team28, Team29, Team30, Team31, Team32;

        private void button3_Click(object sender, EventArgs e)
        {
            Process.Start(teamsFilePath);
        }

        private void SetTeams()
        {
            Team1 = team1txt.Text;
            Team2 = team2txt.Text;
            Team3 = team3txt.Text;
            Team4 = team4txt.Text;
            Team5 = team5txt.Text;
            Team6 = team6txt.Text;
            Team7 = team7txt.Text;
            Team8 = team8txt.Text;
            Team9 = team9txt.Text;
            Team10 = team10txt.Text;
            Team11 = team11txt.Text;
            Team12 = team12txt.Text;
            Team13 = team13txt.Text;
            Team14 = team14txt.Text;
            Team15 = team15txt.Text;
            Team16 = team16txt.Text;
            Team17 = team17txt.Text;
            Team18 = team18txt.Text;
            Team19 = team19txt.Text;
            Team20 = team20txt.Text;
            Team21 = team21txt.Text;
            Team22 = team22txt.Text;
            Team23 = team23txt.Text;
            Team24 = team24txt.Text;
            Team25 = team25txt.Text;
            Team26 = team26txt.Text;
            Team27 = team27txt.Text;
            Team28 = team28txt.Text;
            Team29 = team29txt.Text;
            Team30 = team30txt.Text;
            Team31 = team31txt.Text;
            Team32 = team32txt.Text;
        }

        // adjust conference opponents when changing division 1 conference opponents
        private void div1con_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (div1con.Text)
            {
                case "2":
                    div2con.Text = "1";
                    div3con.Text = "4";
                    div4con.Text = "3";
                    div5con.Text = "6";
                    div6con.Text = "5";
                    div7con.Text = "8";
                    div8con.Text = "7";
                    break;
                case "3":
                    div2con.Text = "4";
                    div3con.Text = "1";
                    div4con.Text = "2";
                    div5con.Text = "7";
                    div6con.Text = "8";
                    div7con.Text = "5";
                    div8con.Text = "6";
                    break;
                case "4":
                    div2con.Text = "3";
                    div3con.Text = "2";
                    div4con.Text = "1";
                    div5con.Text = "8";
                    div6con.Text = "7";
                    div7con.Text = "6";
                    div8con.Text = "5";
                    break;
            }
        }

        // adjust non-conference opponents when changing division 1 non-conference opponents
        private void div1non_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (div1non.Text)
            {
                case "5":
                    div2non.Text = "6";
                    div3non.Text = "7";
                    div4non.Text = "8";
                    div5non.Text = "1";
                    div6non.Text = "2";
                    div7non.Text = "3";
                    div8non.Text = "4";
                    break;
                case "6":
                    div2non.Text = "7";
                    div3non.Text = "8";
                    div4non.Text = "5";
                    div5non.Text = "4";
                    div6non.Text = "1";
                    div7non.Text = "2";
                    div8non.Text = "3";
                    break;
                case "7":
                    div2non.Text = "8";
                    div3non.Text = "5";
                    div4non.Text = "6";
                    div5non.Text = "3";
                    div6non.Text = "4";
                    div7non.Text = "1";
                    div8non.Text = "2";
                    break;
                case "8":
                    div2non.Text = "5";
                    div3non.Text = "6";
                    div4non.Text = "7";
                    div5non.Text = "2";
                    div6non.Text = "3";
                    div7non.Text = "4";
                    div8non.Text = "1";
                    break;
            }
        }

        private List<string> Pre1
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("Week,HomeID,AwayID");
                if (div1non.Text == "5")
                {
                    week.Add("-4," + Team1 + "," + Team21);
                    week.Add("-4," + Team2 + "," + Team22);
                    week.Add("-4," + Team3 + "," + Team23);
                    week.Add("-4," + Team4 + "," + Team24);
                    week.Add("-4," + Team5 + "," + Team25);
                    week.Add("-4," + Team6 + "," + Team26);
                    week.Add("-4," + Team7 + "," + Team27);
                    week.Add("-4," + Team8 + "," + Team28);
                    week.Add("-4," + Team9 + "," + Team29);
                    week.Add("-4," + Team10 + "," + Team30);
                    week.Add("-4," + Team11 + "," + Team31);
                    week.Add("-4," + Team12 + "," + Team32);
                    week.Add("-4," + Team13 + "," + Team17);
                    week.Add("-4," + Team14 + "," + Team18);
                    week.Add("-4," + Team15 + "," + Team19);
                    week.Add("-4," + Team16 + "," + Team20);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("-4," + Team1 + "," + Team25);
                    week.Add("-4," + Team2 + "," + Team26);
                    week.Add("-4," + Team3 + "," + Team27);
                    week.Add("-4," + Team4 + "," + Team28);
                    week.Add("-4," + Team5 + "," + Team29);
                    week.Add("-4," + Team6 + "," + Team30);
                    week.Add("-4," + Team7 + "," + Team31);
                    week.Add("-4," + Team8 + "," + Team32);
                    week.Add("-4," + Team9 + "," + Team17);
                    week.Add("-4," + Team10 + "," + Team18);
                    week.Add("-4," + Team11 + "," + Team19);
                    week.Add("-4," + Team12 + "," + Team20);
                    week.Add("-4," + Team13 + "," + Team21);
                    week.Add("-4," + Team14 + "," + Team22);
                    week.Add("-4," + Team15 + "," + Team23);
                    week.Add("-4," + Team16 + "," + Team24);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("-4," + Team1 + "," + Team29);
                    week.Add("-4," + Team2 + "," + Team30);
                    week.Add("-4," + Team3 + "," + Team31);
                    week.Add("-4," + Team4 + "," + Team32);
                    week.Add("-4," + Team5 + "," + Team17);
                    week.Add("-4," + Team6 + "," + Team18);
                    week.Add("-4," + Team7 + "," + Team19);
                    week.Add("-4," + Team8 + "," + Team20);
                    week.Add("-4," + Team9 + "," + Team21);
                    week.Add("-4," + Team10 + "," + Team22);
                    week.Add("-4," + Team11 + "," + Team23);
                    week.Add("-4," + Team12 + "," + Team24);
                    week.Add("-4," + Team13 + "," + Team25);
                    week.Add("-4," + Team14 + "," + Team26);
                    week.Add("-4," + Team15 + "," + Team27);
                    week.Add("-4," + Team16 + "," + Team28);
                }
                else
                {
                    week.Add("-4," + Team1 + "," + Team17);
                    week.Add("-4," + Team2 + "," + Team18);
                    week.Add("-4," + Team3 + "," + Team19);
                    week.Add("-4," + Team4 + "," + Team20);
                    week.Add("-4," + Team5 + "," + Team21);
                    week.Add("-4," + Team6 + "," + Team22);
                    week.Add("-4," + Team7 + "," + Team23);
                    week.Add("-4," + Team8 + "," + Team24);
                    week.Add("-4," + Team9 + "," + Team25);
                    week.Add("-4," + Team10 + "," + Team26);
                    week.Add("-4," + Team11 + "," + Team27);
                    week.Add("-4," + Team12 + "," + Team28);
                    week.Add("-4," + Team13 + "," + Team29);
                    week.Add("-4," + Team14 + "," + Team30);
                    week.Add("-4," + Team15 + "," + Team31);
                    week.Add("-4," + Team16 + "," + Team32);
                }
                return week;
            }
        }

        private List<string> Pre2
        {
            get
            {
                List<string> week = new List<string>();
                if (div1non.Text == "5")
                {
                    week.Add("-3," + Team25 + "," + Team1);
                    week.Add("-3," + Team26 + "," + Team2);
                    week.Add("-3," + Team27 + "," + Team3);
                    week.Add("-3," + Team28 + "," + Team4);
                    week.Add("-3," + Team29 + "," + Team5);
                    week.Add("-3," + Team30 + "," + Team6);
                    week.Add("-3," + Team31 + "," + Team7);
                    week.Add("-3," + Team32 + "," + Team8);
                    week.Add("-3," + Team17 + "," + Team9);
                    week.Add("-3," + Team18 + "," + Team10);
                    week.Add("-3," + Team19 + "," + Team11);
                    week.Add("-3," + Team20 + "," + Team12);
                    week.Add("-3," + Team21 + "," + Team13);
                    week.Add("-3," + Team22 + "," + Team14);
                    week.Add("-3," + Team23 + "," + Team15);
                    week.Add("-3," + Team24 + "," + Team16);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("-3," + Team29 + "," + Team1);
                    week.Add("-3," + Team30 + "," + Team2);
                    week.Add("-3," + Team31 + "," + Team3);
                    week.Add("-3," + Team32 + "," + Team4);
                    week.Add("-3," + Team17 + "," + Team5);
                    week.Add("-3," + Team18 + "," + Team6);
                    week.Add("-3," + Team19 + "," + Team7);
                    week.Add("-3," + Team20 + "," + Team8);
                    week.Add("-3," + Team21 + "," + Team9);
                    week.Add("-3," + Team22 + "," + Team10);
                    week.Add("-3," + Team23 + "," + Team11);
                    week.Add("-3," + Team24 + "," + Team12);
                    week.Add("-3," + Team25 + "," + Team13);
                    week.Add("-3," + Team26 + "," + Team14);
                    week.Add("-3," + Team27 + "," + Team15);
                    week.Add("-3," + Team28 + "," + Team16);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("-3," + Team17 + "," + Team1);
                    week.Add("-3," + Team18 + "," + Team2);
                    week.Add("-3," + Team19 + "," + Team3);
                    week.Add("-3," + Team20 + "," + Team4);
                    week.Add("-3," + Team21 + "," + Team5);
                    week.Add("-3," + Team22 + "," + Team6);
                    week.Add("-3," + Team23 + "," + Team7);
                    week.Add("-3," + Team24 + "," + Team8);
                    week.Add("-3," + Team25 + "," + Team9);
                    week.Add("-3," + Team26 + "," + Team10);
                    week.Add("-3," + Team27 + "," + Team11);
                    week.Add("-3," + Team28 + "," + Team12);
                    week.Add("-3," + Team29 + "," + Team13);
                    week.Add("-3," + Team30 + "," + Team14);
                    week.Add("-3," + Team31 + "," + Team15);
                    week.Add("-3," + Team32 + "," + Team16);
                }
                else
                {
                    week.Add("-3," + Team21 + "," + Team1);
                    week.Add("-3," + Team22 + "," + Team2);
                    week.Add("-3," + Team23 + "," + Team3);
                    week.Add("-3," + Team24 + "," + Team4);
                    week.Add("-3," + Team25 + "," + Team5);
                    week.Add("-3," + Team26 + "," + Team6);
                    week.Add("-3," + Team27 + "," + Team7);
                    week.Add("-3," + Team28 + "," + Team8);
                    week.Add("-3," + Team29 + "," + Team9);
                    week.Add("-3," + Team30 + "," + Team10);
                    week.Add("-3," + Team31 + "," + Team11);
                    week.Add("-3," + Team32 + "," + Team12);
                    week.Add("-3," + Team17 + "," + Team13);
                    week.Add("-3," + Team18 + "," + Team14);
                    week.Add("-3," + Team19 + "," + Team15);
                    week.Add("-3," + Team20 + "," + Team16);
                }
                return week;
            }
        }

        private List<string> Week1
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("1," + Team1 + "," + Team4);
                week.Add("1," + Team2 + "," + Team3);
                week.Add("1," + Team5 + "," + Team8);
                week.Add("1," + Team6 + "," + Team7);
                week.Add("1," + Team9 + "," + Team12);
                week.Add("1," + Team10 + "," + Team11);
                week.Add("1," + Team13 + "," + Team16);
                week.Add("1," + Team14 + "," + Team15);
                week.Add("1," + Team17 + "," + Team20);
                week.Add("1," + Team18 + "," + Team19);
                week.Add("1," + Team21 + "," + Team24);
                week.Add("1," + Team22 + "," + Team23);
                week.Add("1," + Team25 + "," + Team28);
                week.Add("1," + Team26 + "," + Team27);
                week.Add("1," + Team29 + "," + Team32);
                week.Add("1," + Team30 + "," + Team31);
                return week;
            }
        }

        private List<string> Week2
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("2," + Team13 + "," + Team1);
                    week.Add("2," + Team14 + "," + Team2);
                    week.Add("2," + Team15 + "," + Team3);
                    week.Add("2," + Team16 + "," + Team4);
                    week.Add("2," + Team5 + "," + Team9);
                    week.Add("2," + Team6 + "," + Team10);
                    week.Add("2," + Team7 + "," + Team11);
                    week.Add("2," + Team8 + "," + Team12);
                    week.Add("2," + Team29 + "," + Team17);
                    week.Add("2," + Team30 + "," + Team18);
                    week.Add("2," + Team31 + "," + Team19);
                    week.Add("2," + Team32 + "," + Team20);
                    week.Add("2," + Team21 + "," + Team25);
                    week.Add("2," + Team22 + "," + Team26);
                    week.Add("2," + Team23 + "," + Team27);
                    week.Add("2," + Team24 + "," + Team28);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("2," + Team9 + "," + Team1);
                    week.Add("2," + Team10 + "," + Team2);
                    week.Add("2," + Team11 + "," + Team3);
                    week.Add("2," + Team12 + "," + Team4);
                    week.Add("2," + Team5 + "," + Team13);
                    week.Add("2," + Team6 + "," + Team14);
                    week.Add("2," + Team7 + "," + Team15);
                    week.Add("2," + Team8 + "," + Team16);
                    week.Add("2," + Team25 + "," + Team17);
                    week.Add("2," + Team26 + "," + Team18);
                    week.Add("2," + Team27 + "," + Team19);
                    week.Add("2," + Team28 + "," + Team20);
                    week.Add("2," + Team21 + "," + Team29);
                    week.Add("2," + Team22 + "," + Team30);
                    week.Add("2," + Team23 + "," + Team31);
                    week.Add("2," + Team24 + "," + Team32);
                }
                else // 1v2 and 5v6
                {
                    week.Add("2," + Team1 + "," + Team5);
                    week.Add("2," + Team2 + "," + Team6);
                    week.Add("2," + Team3 + "," + Team7);
                    week.Add("2," + Team4 + "," + Team8);
                    week.Add("2," + Team9 + "," + Team13);
                    week.Add("2," + Team10 + "," + Team14);
                    week.Add("2," + Team11 + "," + Team15);
                    week.Add("2," + Team12 + "," + Team16);
                    week.Add("2," + Team21 + "," + Team17);
                    week.Add("2," + Team22 + "," + Team18);
                    week.Add("2," + Team23 + "," + Team19);
                    week.Add("2," + Team24 + "," + Team20);
                    week.Add("2," + Team29 + "," + Team25);
                    week.Add("2," + Team30 + "," + Team26);
                    week.Add("2," + Team31 + "," + Team27);
                    week.Add("2," + Team32 + "," + Team28);
                }
                return week;                
            }
        }

        private List<string> Week3
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("3," + Team1 + "," + Team14);
                    week.Add("3," + Team2 + "," + Team15);
                    week.Add("3," + Team3 + "," + Team16);
                    week.Add("3," + Team4 + "," + Team13);
                    week.Add("3," + Team10 + "," + Team5);
                    week.Add("3," + Team11 + "," + Team6);
                    week.Add("3," + Team12 + "," + Team7);
                    week.Add("3," + Team9 + "," + Team8);
                    week.Add("3," + Team17 + "," + Team30);
                    week.Add("3," + Team18 + "," + Team31);
                    week.Add("3," + Team19 + "," + Team32);
                    week.Add("3," + Team20 + "," + Team29);
                    week.Add("3," + Team26 + "," + Team21);
                    week.Add("3," + Team27 + "," + Team22);
                    week.Add("3," + Team28 + "," + Team23);
                    week.Add("3," + Team25 + "," + Team24);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("3," + Team1 + "," + Team10);
                    week.Add("3," + Team2 + "," + Team11);
                    week.Add("3," + Team3 + "," + Team12);
                    week.Add("3," + Team4 + "," + Team9);
                    week.Add("3," + Team13 + "," + Team8);
                    week.Add("3," + Team14 + "," + Team5);
                    week.Add("3," + Team15 + "," + Team6);
                    week.Add("3," + Team16 + "," + Team7);
                    week.Add("3," + Team17 + "," + Team26);
                    week.Add("3," + Team18 + "," + Team27);
                    week.Add("3," + Team19 + "," + Team28);
                    week.Add("3," + Team20 + "," + Team25);
                    week.Add("3," + Team29 + "," + Team24);
                    week.Add("3," + Team30 + "," + Team21);
                    week.Add("3," + Team31 + "," + Team22);
                    week.Add("3," + Team32 + "," + Team23);
                }
                else // 1v2 and 5v6
                {
                    week.Add("3," + Team5 + "," + Team4);
                    week.Add("3," + Team6 + "," + Team1);
                    week.Add("3," + Team7 + "," + Team2);
                    week.Add("3," + Team8 + "," + Team3);
                    week.Add("3," + Team13 + "," + Team12);
                    week.Add("3," + Team14 + "," + Team9);
                    week.Add("3," + Team15 + "," + Team10);
                    week.Add("3," + Team16 + "," + Team11);
                    week.Add("3," + Team17 + "," + Team22);
                    week.Add("3," + Team18 + "," + Team23);
                    week.Add("3," + Team19 + "," + Team24);
                    week.Add("3," + Team20 + "," + Team21);
                    week.Add("3," + Team25 + "," + Team30);
                    week.Add("3," + Team26 + "," + Team31);
                    week.Add("3," + Team27 + "," + Team32);
                    week.Add("3," + Team28 + "," + Team29);
                }
                return week;
            }
        }

        private List<string> Week4
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("4," + Team1 + "," + Team2);
                week.Add("4," + Team4 + "," + Team3);
                week.Add("4," + Team5 + "," + Team6);
                week.Add("4," + Team8 + "," + Team7);
                week.Add("4," + Team9 + "," + Team10);
                week.Add("4," + Team12 + "," + Team11);
                week.Add("4," + Team13 + "," + Team14);
                week.Add("4," + Team16 + "," + Team15);
                week.Add("4," + Team17 + "," + Team18);
                week.Add("4," + Team20 + "," + Team19);
                week.Add("4," + Team21 + "," + Team22);
                week.Add("4," + Team24 + "," + Team23);
                week.Add("4," + Team25 + "," + Team26);
                week.Add("4," + Team28 + "," + Team27);
                week.Add("4," + Team29 + "," + Team30);
                week.Add("4," + Team32 + "," + Team31);
                return week;
            }
        }

        private List<string> Week5
        {
            get
            {
                List<string> week = new List<string>();
                if (div1non.Text == "5")
                {
                    week.Add("5," + Team5 + "," + Team22);
                    week.Add("5," + Team6 + "," + Team23);
                    week.Add("5," + Team7 + "," + Team24);
                    week.Add("5," + Team8 + "," + Team21);
                    week.Add("5," + Team13 + "," + Team30);
                    week.Add("5," + Team14 + "," + Team31);
                    week.Add("5," + Team15 + "," + Team32);
                    week.Add("5," + Team16 + "," + Team29);
                    week.Add("5," + Team17 + "," + Team4);
                    week.Add("5," + Team18 + "," + Team1);
                    week.Add("5," + Team19 + "," + Team2);
                    week.Add("5," + Team20 + "," + Team3);
                    week.Add("5," + Team25 + "," + Team12);
                    week.Add("5," + Team26 + "," + Team9);
                    week.Add("5," + Team27 + "," + Team10);
                    week.Add("5," + Team28 + "," + Team11);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("5," + Team5 + "," + Team26);
                    week.Add("5," + Team6 + "," + Team27);
                    week.Add("5," + Team7 + "," + Team28);
                    week.Add("5," + Team8 + "," + Team25);
                    week.Add("5," + Team13 + "," + Team18);
                    week.Add("5," + Team14 + "," + Team19);
                    week.Add("5," + Team15 + "," + Team20);
                    week.Add("5," + Team16 + "," + Team17);
                    week.Add("5," + Team21 + "," + Team4);
                    week.Add("5," + Team22 + "," + Team1);
                    week.Add("5," + Team23 + "," + Team2);
                    week.Add("5," + Team24 + "," + Team3);
                    week.Add("5," + Team29 + "," + Team12);
                    week.Add("5," + Team30 + "," + Team9);
                    week.Add("5," + Team31 + "," + Team10);
                    week.Add("5," + Team32 + "," + Team11);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("5," + Team1 + "," + Team25);
                    week.Add("5," + Team2 + "," + Team26);
                    week.Add("5," + Team3 + "," + Team27);
                    week.Add("5," + Team4 + "," + Team28);
                    week.Add("5," + Team9 + "," + Team17);
                    week.Add("5," + Team10 + "," + Team18);
                    week.Add("5," + Team11 + "," + Team19);
                    week.Add("5," + Team12 + "," + Team20);
                    week.Add("5," + Team21 + "," + Team13);
                    week.Add("5," + Team22 + "," + Team14);
                    week.Add("5," + Team23 + "," + Team15);
                    week.Add("5," + Team24 + "," + Team16);
                    week.Add("5," + Team29 + "," + Team5);
                    week.Add("5," + Team30 + "," + Team6);
                    week.Add("5," + Team31 + "," + Team7);
                    week.Add("5," + Team32 + "," + Team8);
                }
                else
                {
                    week.Add("5," + Team1 + "," + Team29);
                    week.Add("5," + Team2 + "," + Team30);
                    week.Add("5," + Team3 + "," + Team31);
                    week.Add("5," + Team4 + "," + Team32);
                    week.Add("5," + Team9 + "," + Team21);
                    week.Add("5," + Team10 + "," + Team22);
                    week.Add("5," + Team11 + "," + Team23);
                    week.Add("5," + Team12 + "," + Team24);
                    week.Add("5," + Team17 + "," + Team5);
                    week.Add("5," + Team18 + "," + Team6);
                    week.Add("5," + Team19 + "," + Team7);
                    week.Add("5," + Team20 + "," + Team8);
                    week.Add("5," + Team25 + "," + Team13);
                    week.Add("5," + Team26 + "," + Team14);
                    week.Add("5," + Team27 + "," + Team15);
                    week.Add("5," + Team28 + "," + Team16);
                }
                return week;
            }
        }

        private List<string> Week6
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("6," + Team5 + "," + Team11);
                    week.Add("6," + Team6 + "," + Team12);
                    week.Add("6," + Team7 + "," + Team9);
                    week.Add("6," + Team8 + "," + Team10);
                    week.Add("6," + Team13 + "," + Team3);
                    week.Add("6," + Team14 + "," + Team4);
                    week.Add("6," + Team15 + "," + Team1);
                    week.Add("6," + Team16 + "," + Team2);
                    week.Add("6," + Team21 + "," + Team27);
                    week.Add("6," + Team22 + "," + Team28);
                    week.Add("6," + Team23 + "," + Team25);
                    week.Add("6," + Team24 + "," + Team26);
                    week.Add("6," + Team29 + "," + Team19);
                    week.Add("6," + Team30 + "," + Team20);
                    week.Add("6," + Team31 + "," + Team17);
                    week.Add("6," + Team32 + "," + Team18);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("6," + Team5 + "," + Team15);
                    week.Add("6," + Team6 + "," + Team16);
                    week.Add("6," + Team7 + "," + Team13);
                    week.Add("6," + Team8 + "," + Team14);
                    week.Add("6," + Team9 + "," + Team3);
                    week.Add("6," + Team10 + "," + Team4);
                    week.Add("6," + Team11 + "," + Team1);
                    week.Add("6," + Team12 + "," + Team2);
                    week.Add("6," + Team21 + "," + Team31);
                    week.Add("6," + Team22 + "," + Team32);
                    week.Add("6," + Team23 + "," + Team29);
                    week.Add("6," + Team24 + "," + Team30);
                    week.Add("6," + Team25 + "," + Team19);
                    week.Add("6," + Team26 + "," + Team20);
                    week.Add("6," + Team27 + "," + Team17);
                    week.Add("6," + Team28 + "," + Team18);
                }
                else // 1v2 and 5v6
                {
                    week.Add("6," + Team1 + "," + Team7);
                    week.Add("6," + Team2 + "," + Team8);
                    week.Add("6," + Team3 + "," + Team5);
                    week.Add("6," + Team4 + "," + Team6);
                    week.Add("6," + Team9 + "," + Team15);
                    week.Add("6," + Team10 + "," + Team16);
                    week.Add("6," + Team11 + "," + Team13);
                    week.Add("6," + Team12 + "," + Team14);
                    week.Add("6," + Team21 + "," + Team19);
                    week.Add("6," + Team22 + "," + Team20);
                    week.Add("6," + Team23 + "," + Team17);
                    week.Add("6," + Team24 + "," + Team18);
                    week.Add("6," + Team29 + "," + Team27);
                    week.Add("6," + Team30 + "," + Team28);
                    week.Add("6," + Team31 + "," + Team25);
                    week.Add("6," + Team32 + "," + Team26);
                }
                return week;
            }
        }

        private List<string> Week7
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("7," + Team1 + "," + Team16);
                    week.Add("7," + Team2 + "," + Team13);
                    week.Add("7," + Team3 + "," + Team14);
                    week.Add("7," + Team4 + "," + Team15);
                    week.Add("7," + Team9 + "," + Team6);
                    week.Add("7," + Team10 + "," + Team7);
                    week.Add("7," + Team11 + "," + Team8);
                    week.Add("7," + Team12 + "," + Team5);
                    week.Add("7," + Team17 + "," + Team32);
                    week.Add("7," + Team18 + "," + Team29);
                    week.Add("7," + Team19 + "," + Team30);
                    week.Add("7," + Team20 + "," + Team31);
                    week.Add("7," + Team25 + "," + Team22);
                    week.Add("7," + Team26 + "," + Team23);
                    week.Add("7," + Team27 + "," + Team24);
                    week.Add("7," + Team28 + "," + Team21);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("7," + Team1 + "," + Team12);
                    week.Add("7," + Team2 + "," + Team9);
                    week.Add("7," + Team3 + "," + Team10);
                    week.Add("7," + Team4 + "," + Team11);
                    week.Add("7," + Team13 + "," + Team6);
                    week.Add("7," + Team14 + "," + Team7);
                    week.Add("7," + Team15 + "," + Team8);
                    week.Add("7," + Team16 + "," + Team5);
                    week.Add("7," + Team17 + "," + Team28);
                    week.Add("7," + Team18 + "," + Team25);
                    week.Add("7," + Team19 + "," + Team26);
                    week.Add("7," + Team20 + "," + Team27);
                    week.Add("7," + Team29 + "," + Team22);
                    week.Add("7," + Team30 + "," + Team23);
                    week.Add("7," + Team31 + "," + Team24);
                    week.Add("7," + Team32 + "," + Team21);
                }
                else // 1v2 and 5v6
                {
                    week.Add("7," + Team5 + "," + Team2);
                    week.Add("7," + Team6 + "," + Team3);
                    week.Add("7," + Team7 + "," + Team4);
                    week.Add("7," + Team8 + "," + Team1);
                    week.Add("7," + Team13 + "," + Team10);
                    week.Add("7," + Team14 + "," + Team11);
                    week.Add("7," + Team15 + "," + Team12);
                    week.Add("7," + Team16 + "," + Team9);
                    week.Add("7," + Team17 + "," + Team24);
                    week.Add("7," + Team18 + "," + Team21);
                    week.Add("7," + Team19 + "," + Team22);
                    week.Add("7," + Team20 + "," + Team23);
                    week.Add("7," + Team25 + "," + Team32);
                    week.Add("7," + Team26 + "," + Team29);
                    week.Add("7," + Team27 + "," + Team30);
                    week.Add("7," + Team28 + "," + Team31);
                }
                return week;
            }
        }

        private List<string> Week8
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("8," + Team2 + "," + Team4);
                week.Add("8," + Team3 + "," + Team1);
                week.Add("8," + Team6 + "," + Team8);
                week.Add("8," + Team7 + "," + Team5);
                week.Add("8," + Team10 + "," + Team12);
                week.Add("8," + Team11 + "," + Team9);
                week.Add("8," + Team14 + "," + Team16);
                week.Add("8," + Team15 + "," + Team13);
                week.Add("8," + Team18 + "," + Team20);
                week.Add("8," + Team19 + "," + Team17);
                week.Add("8," + Team22 + "," + Team24);
                week.Add("8," + Team23 + "," + Team21);
                week.Add("8," + Team26 + "," + Team28);
                week.Add("8," + Team27 + "," + Team25);
                week.Add("8," + Team30 + "," + Team32);
                week.Add("8," + Team31 + "," + Team29);
                return week;
            }
        }

        private List<string> Week9
        {
            get
            {
                List<string> week = new List<string>();
                if (div1non.Text == "5")
                {
                    week.Add("9," + Team5 + "," + Team24);
                    week.Add("9," + Team6 + "," + Team21);
                    week.Add("9," + Team7 + "," + Team22);
                    week.Add("9," + Team8 + "," + Team23);
                    week.Add("9," + Team13 + "," + Team32);
                    week.Add("9," + Team14 + "," + Team29);
                    week.Add("9," + Team15 + "," + Team30);
                    week.Add("9," + Team16 + "," + Team31);
                    week.Add("9," + Team17 + "," + Team2);
                    week.Add("9," + Team18 + "," + Team3);
                    week.Add("9," + Team19 + "," + Team4);
                    week.Add("9," + Team20 + "," + Team1);
                    week.Add("9," + Team25 + "," + Team10);
                    week.Add("9," + Team26 + "," + Team11);
                    week.Add("9," + Team27 + "," + Team12);
                    week.Add("9," + Team28 + "," + Team9);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("9," + Team5 + "," + Team28);
                    week.Add("9," + Team6 + "," + Team25);
                    week.Add("9," + Team7 + "," + Team26);
                    week.Add("9," + Team8 + "," + Team27);
                    week.Add("9," + Team13 + "," + Team20);
                    week.Add("9," + Team14 + "," + Team17);
                    week.Add("9," + Team15 + "," + Team18);
                    week.Add("9," + Team16 + "," + Team19);
                    week.Add("9," + Team21 + "," + Team2);
                    week.Add("9," + Team22 + "," + Team3);
                    week.Add("9," + Team23 + "," + Team4);
                    week.Add("9," + Team24 + "," + Team1);
                    week.Add("9," + Team29 + "," + Team10);
                    week.Add("9," + Team30 + "," + Team11);
                    week.Add("9," + Team31 + "," + Team12);
                    week.Add("9," + Team32 + "," + Team9);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("9," + Team1 + "," + Team27);
                    week.Add("9," + Team2 + "," + Team28);
                    week.Add("9," + Team3 + "," + Team25);
                    week.Add("9," + Team4 + "," + Team26);
                    week.Add("9," + Team9 + "," + Team19);
                    week.Add("9," + Team10 + "," + Team20);
                    week.Add("9," + Team11 + "," + Team17);
                    week.Add("9," + Team12 + "," + Team18);
                    week.Add("9," + Team21 + "," + Team15);
                    week.Add("9," + Team22 + "," + Team16);
                    week.Add("9," + Team23 + "," + Team13);
                    week.Add("9," + Team24 + "," + Team14);
                    week.Add("9," + Team29 + "," + Team7);
                    week.Add("9," + Team30 + "," + Team8);
                    week.Add("9," + Team31 + "," + Team5);
                    week.Add("9," + Team32 + "," + Team6);
                }
                else
                {
                    week.Add("9," + Team1 + "," + Team32);
                    week.Add("9," + Team2 + "," + Team29);
                    week.Add("9," + Team3 + "," + Team30);
                    week.Add("9," + Team4 + "," + Team31);
                    week.Add("9," + Team13 + "," + Team28);
                    week.Add("9," + Team14 + "," + Team25);
                    week.Add("9," + Team15 + "," + Team26);
                    week.Add("9," + Team16 + "," + Team27);
                    week.Add("9," + Team17 + "," + Team6);
                    week.Add("9," + Team18 + "," + Team7);
                    week.Add("9," + Team19 + "," + Team8);
                    week.Add("9," + Team20 + "," + Team5);
                    week.Add("9," + Team21 + "," + Team10);
                    week.Add("9," + Team22 + "," + Team11);
                    week.Add("9," + Team23 + "," + Team12);
                    week.Add("9," + Team24 + "," + Team9);
                }
                return week;
            }
        }

        private List<string> Week10
        {
            get
            {
                List<string> week = new List<string>();
                if (div1non.Text == "5")
                {
                    week.Add("10," + Team1 + "," + Team17);
                    week.Add("10," + Team2 + "," + Team18);
                    week.Add("10," + Team3 + "," + Team19);
                    week.Add("10," + Team4 + "," + Team20);
                    week.Add("10," + Team9 + "," + Team25);
                    week.Add("10," + Team10 + "," + Team26);
                    week.Add("10," + Team11 + "," + Team27);
                    week.Add("10," + Team12 + "," + Team28);
                    week.Add("10," + Team21 + "," + Team5);
                    week.Add("10," + Team22 + "," + Team6);
                    week.Add("10," + Team23 + "," + Team7);
                    week.Add("10," + Team24 + "," + Team8);
                    week.Add("10," + Team29 + "," + Team13);
                    week.Add("10," + Team30 + "," + Team14);
                    week.Add("10," + Team31 + "," + Team15);
                    week.Add("10," + Team32 + "," + Team16);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("10," + Team1 + "," + Team21);
                    week.Add("10," + Team2 + "," + Team22);
                    week.Add("10," + Team3 + "," + Team23);
                    week.Add("10," + Team4 + "," + Team24);
                    week.Add("10," + Team9 + "," + Team29);
                    week.Add("10," + Team10 + "," + Team30);
                    week.Add("10," + Team11 + "," + Team31);
                    week.Add("10," + Team12 + "," + Team32);
                    week.Add("10," + Team17 + "," + Team13);
                    week.Add("10," + Team18 + "," + Team14);
                    week.Add("10," + Team19 + "," + Team15);
                    week.Add("10," + Team20 + "," + Team16);
                    week.Add("10," + Team25 + "," + Team5);
                    week.Add("10," + Team26 + "," + Team6);
                    week.Add("10," + Team27 + "," + Team7);
                    week.Add("10," + Team28 + "," + Team8);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("10," + Team5 + "," + Team30);
                    week.Add("10," + Team6 + "," + Team31);
                    week.Add("10," + Team7 + "," + Team32);
                    week.Add("10," + Team8 + "," + Team29);
                    week.Add("10," + Team13 + "," + Team22);
                    week.Add("10," + Team14 + "," + Team23);
                    week.Add("10," + Team15 + "," + Team24);
                    week.Add("10," + Team16 + "," + Team21);
                    week.Add("10," + Team17 + "," + Team12);
                    week.Add("10," + Team18 + "," + Team9);
                    week.Add("10," + Team19 + "," + Team10);
                    week.Add("10," + Team20 + "," + Team11);
                    week.Add("10," + Team25 + "," + Team4);
                    week.Add("10," + Team26 + "," + Team1);
                    week.Add("10," + Team27 + "," + Team2);
                    week.Add("10," + Team28 + "," + Team3);
                }
                else
                {
                    week.Add("10," + Team5 + "," + Team19);
                    week.Add("10," + Team6 + "," + Team20);
                    week.Add("10," + Team7 + "," + Team17);
                    week.Add("10," + Team8 + "," + Team18);
                    week.Add("10," + Team9 + "," + Team23);
                    week.Add("10," + Team10 + "," + Team24);
                    week.Add("10," + Team11 + "," + Team21);
                    week.Add("10," + Team12 + "," + Team22);
                    week.Add("10," + Team25 + "," + Team15);
                    week.Add("10," + Team26 + "," + Team16);
                    week.Add("10," + Team27 + "," + Team13);
                    week.Add("10," + Team28 + "," + Team14);
                    week.Add("10," + Team29 + "," + Team3);
                    week.Add("10," + Team30 + "," + Team4);
                    week.Add("10," + Team31 + "," + Team1);
                    week.Add("10," + Team32 + "," + Team2);
                }
                return week;
            }
        }

        private List<string> Week11
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("11," + Team1 + "," + Team9);
                    week.Add("11," + Team2 + "," + Team10);
                    week.Add("11," + Team3 + "," + Team11);
                    week.Add("11," + Team4 + "," + Team12);
                    week.Add("11," + Team13 + "," + Team5);
                    week.Add("11," + Team14 + "," + Team6);
                    week.Add("11," + Team15 + "," + Team7);
                    week.Add("11," + Team16 + "," + Team8);
                    week.Add("11," + Team17 + "," + Team25);
                    week.Add("11," + Team18 + "," + Team26);
                    week.Add("11," + Team19 + "," + Team27);
                    week.Add("11," + Team20 + "," + Team28);
                    week.Add("11," + Team29 + "," + Team21);
                    week.Add("11," + Team30 + "," + Team22);
                    week.Add("11," + Team31 + "," + Team23);
                    week.Add("11," + Team32 + "," + Team24);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("11," + Team1 + "," + Team5);
                    week.Add("11," + Team2 + "," + Team6);
                    week.Add("11," + Team3 + "," + Team7);
                    week.Add("11," + Team4 + "," + Team8);
                    week.Add("11," + Team9 + "," + Team13);
                    week.Add("11," + Team10 + "," + Team14);
                    week.Add("11," + Team11 + "," + Team15);
                    week.Add("11," + Team12 + "," + Team16);
                    week.Add("11," + Team17 + "," + Team21);
                    week.Add("11," + Team18 + "," + Team22);
                    week.Add("11," + Team19 + "," + Team23);
                    week.Add("11," + Team20 + "," + Team24);
                    week.Add("11," + Team25 + "," + Team29);
                    week.Add("11," + Team26 + "," + Team30);
                    week.Add("11," + Team27 + "," + Team31);
                    week.Add("11," + Team28 + "," + Team32);
                }
                else // 1v2 and 5v6
                {
                    week.Add("11," + Team9 + "," + Team5);
                    week.Add("11," + Team10 + "," + Team6);
                    week.Add("11," + Team11 + "," + Team7);
                    week.Add("11," + Team12 + "," + Team8);
                    week.Add("11," + Team13 + "," + Team1);
                    week.Add("11," + Team14 + "," + Team2);
                    week.Add("11," + Team15 + "," + Team3);
                    week.Add("11," + Team16 + "," + Team4);
                    week.Add("11," + Team17 + "," + Team29);
                    week.Add("11," + Team18 + "," + Team30);
                    week.Add("11," + Team19 + "," + Team31);
                    week.Add("11," + Team20 + "," + Team32);
                    week.Add("11," + Team21 + "," + Team25);
                    week.Add("11," + Team22 + "," + Team26);
                    week.Add("11," + Team23 + "," + Team27);
                    week.Add("11," + Team24 + "," + Team28);
                }
                return week;
            }
        }

        private List<string> Week12
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("12," + Team3 + "," + Team2);
                week.Add("12," + Team4 + "," + Team1);
                week.Add("12," + Team7 + "," + Team6);
                week.Add("12," + Team8 + "," + Team5);
                week.Add("12," + Team11 + "," + Team10);
                week.Add("12," + Team12 + "," + Team9);
                week.Add("12," + Team15 + "," + Team14);
                week.Add("12," + Team16 + "," + Team13);
                week.Add("12," + Team19 + "," + Team18);
                week.Add("12," + Team20 + "," + Team17);
                week.Add("12," + Team23 + "," + Team22);
                week.Add("12," + Team24 + "," + Team21);
                week.Add("12," + Team27 + "," + Team26);
                week.Add("12," + Team28 + "," + Team25);
                week.Add("12," + Team31 + "," + Team30);
                week.Add("12," + Team32 + "," + Team29);
                return week;
            }
        }

        private List<string> Week13
        {
            get
            {
                List<string> week = new List<string>();
                if (div1con.Text == "4" && div5con.Text == "8") // 2v3, 6v7
                {
                    week.Add("13," + Team5 + "," + Team1);
                    week.Add("13," + Team6 + "," + Team2);
                    week.Add("13," + Team7 + "," + Team3);
                    week.Add("13," + Team8 + "," + Team4);
                    week.Add("13," + Team9 + "," + Team13);
                    week.Add("13," + Team10 + "," + Team14);
                    week.Add("13," + Team11 + "," + Team15);
                    week.Add("13," + Team12 + "," + Team16);
                    week.Add("13," + Team21 + "," + Team17);
                    week.Add("13," + Team22 + "," + Team18);
                    week.Add("13," + Team23 + "," + Team19);
                    week.Add("13," + Team24 + "," + Team20);
                    week.Add("13," + Team25 + "," + Team29);
                    week.Add("13," + Team26 + "," + Team30);
                    week.Add("13," + Team27 + "," + Team31);
                    week.Add("13," + Team28 + "," + Team32);
                }
                else if (div1con.Text == "3" && div5con.Text == "7") // 2v4, 6v8
                {
                    week.Add("13," + Team5 + "," + Team9);
                    week.Add("13," + Team6 + "," + Team10);
                    week.Add("13," + Team7 + "," + Team11);
                    week.Add("13," + Team8 + "," + Team12);
                    week.Add("13," + Team13 + "," + Team1);
                    week.Add("13," + Team14 + "," + Team2);
                    week.Add("13," + Team15 + "," + Team3);
                    week.Add("13," + Team16 + "," + Team4);
                    week.Add("13," + Team21 + "," + Team25);
                    week.Add("13," + Team22 + "," + Team26);
                    week.Add("13," + Team23 + "," + Team27);
                    week.Add("13," + Team24 + "," + Team28);
                    week.Add("13," + Team29 + "," + Team17);
                    week.Add("13," + Team30 + "," + Team18);
                    week.Add("13," + Team31 + "," + Team19);
                    week.Add("13," + Team32 + "," + Team20);
                }
                else // 1v2 and 5v6
                {
                    week.Add("13," + Team1 + "," + Team9);
                    week.Add("13," + Team2 + "," + Team10);
                    week.Add("13," + Team3 + "," + Team11);
                    week.Add("13," + Team4 + "," + Team12);
                    week.Add("13," + Team5 + "," + Team13);
                    week.Add("13," + Team6 + "," + Team14);
                    week.Add("13," + Team7 + "," + Team15);
                    week.Add("13," + Team8 + "," + Team16);
                    week.Add("13," + Team25 + "," + Team17);
                    week.Add("13," + Team26 + "," + Team18);
                    week.Add("13," + Team27 + "," + Team19);
                    week.Add("13," + Team28 + "," + Team20);
                    week.Add("13," + Team29 + "," + Team21);
                    week.Add("13," + Team30 + "," + Team22);
                    week.Add("13," + Team31 + "," + Team23);
                    week.Add("13," + Team32 + "," + Team24);
                }
                return week;
            }
        }

        private List<string> Week14
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("14," + Team1 + "," + Team3);
                week.Add("14," + Team4 + "," + Team2);
                week.Add("14," + Team5 + "," + Team7);
                week.Add("14," + Team8 + "," + Team6);
                week.Add("14," + Team9 + "," + Team11);
                week.Add("14," + Team12 + "," + Team10);
                week.Add("14," + Team13 + "," + Team15);
                week.Add("14," + Team16 + "," + Team14);
                week.Add("14," + Team17 + "," + Team19);
                week.Add("14," + Team20 + "," + Team18);
                week.Add("14," + Team21 + "," + Team23);
                week.Add("14," + Team24 + "," + Team22);
                week.Add("14," + Team25 + "," + Team27);
                week.Add("14," + Team28 + "," + Team26);
                week.Add("14," + Team29 + "," + Team31);
                week.Add("14," + Team32 + "," + Team30);
                return week;
            }
        }

        private List<string> Week15
        {
            get
            {
                List<string> week = new List<string>();
                if (div1non.Text == "5")
                {
                    week.Add("15," + Team1 + "," + Team19);
                    week.Add("15," + Team2 + "," + Team20);
                    week.Add("15," + Team3 + "," + Team17);
                    week.Add("15," + Team4 + "," + Team18);
                    week.Add("15," + Team9 + "," + Team27);
                    week.Add("15," + Team10 + "," + Team28);
                    week.Add("15," + Team11 + "," + Team25);
                    week.Add("15," + Team12 + "," + Team26);
                    week.Add("15," + Team21 + "," + Team7);
                    week.Add("15," + Team22 + "," + Team8);
                    week.Add("15," + Team23 + "," + Team5);
                    week.Add("15," + Team24 + "," + Team6);
                    week.Add("15," + Team29 + "," + Team15);
                    week.Add("15," + Team30 + "," + Team16);
                    week.Add("15," + Team31 + "," + Team13);
                    week.Add("15," + Team32 + "," + Team14);
                }
                else if (div1non.Text == "6")
                {
                    week.Add("15," + Team1 + "," + Team23);
                    week.Add("15," + Team2 + "," + Team24);
                    week.Add("15," + Team3 + "," + Team21);
                    week.Add("15," + Team4 + "," + Team22);
                    week.Add("15," + Team9 + "," + Team31);
                    week.Add("15," + Team10 + "," + Team32);
                    week.Add("15," + Team11 + "," + Team29);
                    week.Add("15," + Team12 + "," + Team30);
                    week.Add("15," + Team17 + "," + Team15);
                    week.Add("15," + Team18 + "," + Team16);
                    week.Add("15," + Team19 + "," + Team13);
                    week.Add("15," + Team20 + "," + Team14);
                    week.Add("15," + Team25 + "," + Team7);
                    week.Add("15," + Team26 + "," + Team8);
                    week.Add("15," + Team27 + "," + Team5);
                    week.Add("15," + Team28 + "," + Team6);
                }
                else if (div1non.Text == "7")
                {
                    week.Add("15," + Team5 + "," + Team32);
                    week.Add("15," + Team6 + "," + Team29);
                    week.Add("15," + Team7 + "," + Team30);
                    week.Add("15," + Team8 + "," + Team31);
                    week.Add("15," + Team13 + "," + Team24);
                    week.Add("15," + Team14 + "," + Team21);
                    week.Add("15," + Team15 + "," + Team22);
                    week.Add("15," + Team16 + "," + Team23);
                    week.Add("15," + Team17 + "," + Team10);
                    week.Add("15," + Team18 + "," + Team11);
                    week.Add("15," + Team19 + "," + Team12);
                    week.Add("15," + Team20 + "," + Team9);
                    week.Add("15," + Team25 + "," + Team2);
                    week.Add("15," + Team26 + "," + Team3);
                    week.Add("15," + Team27 + "," + Team4);
                    week.Add("15," + Team28 + "," + Team1);
                }
                else
                {
                    week.Add("15," + Team5 + "," + Team18);
                    week.Add("15," + Team6 + "," + Team19);
                    week.Add("15," + Team7 + "," + Team20);
                    week.Add("15," + Team8 + "," + Team17);
                    week.Add("15," + Team13 + "," + Team26);
                    week.Add("15," + Team14 + "," + Team27);
                    week.Add("15," + Team15 + "," + Team28);
                    week.Add("15," + Team16 + "," + Team25);
                    week.Add("15," + Team21 + "," + Team12);
                    week.Add("15," + Team22 + "," + Team9);
                    week.Add("15," + Team23 + "," + Team10);
                    week.Add("15," + Team24 + "," + Team11);
                    week.Add("15," + Team29 + "," + Team4);
                    week.Add("15," + Team30 + "," + Team1);
                    week.Add("15," + Team31 + "," + Team2);
                    week.Add("15," + Team32 + "," + Team3);
                }
                return week;
            }
        }

        private List<string> Week16
        {
            get
            {
                List<string> week = new List<string>();
                week.Add("16," + Team2 + "," + Team1);
                week.Add("16," + Team3 + "," + Team4);
                week.Add("16," + Team6 + "," + Team5);
                week.Add("16," + Team7 + "," + Team8);
                week.Add("16," + Team10 + "," + Team9);
                week.Add("16," + Team11 + "," + Team12);
                week.Add("16," + Team14 + "," + Team13);
                week.Add("16," + Team15 + "," + Team16);
                week.Add("16," + Team18 + "," + Team17);
                week.Add("16," + Team19 + "," + Team20);
                week.Add("16," + Team22 + "," + Team21);
                week.Add("16," + Team23 + "," + Team24);
                week.Add("16," + Team26 + "," + Team25);
                week.Add("16," + Team27 + "," + Team28);
                week.Add("16," + Team30 + "," + Team29);
                week.Add("16," + Team31 + "," + Team32);
                return week;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }
    }
}
